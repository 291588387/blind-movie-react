import { Outlet, useNavigate } from "react-router-dom";
import { HomeOutlined, QuestionOutlined, CheckOutlined } from '@ant-design/icons'
import { Menu } from "antd";
import { useState } from "react";
const Layout = () => {
  const navItems = [
    {
      label: 'Home',
      key: '/',
      icon: <HomeOutlined />,
    },
    {
      label: 'About',
      key: '/about',
      icon: <QuestionOutlined />,
    },
    {
      label: 'Done',
      key: '/done',
      icon: <CheckOutlined />,
    }
  ];
  const [currentKey, setCurrentKey] = useState('/');
  const navigate = useNavigate();
  const handleMenuChange = (event) => {
    navigate(event.key);
    setCurrentKey(event.key);
  }
  return (
    <div className="header">
      <Menu
        mode="horizontal"
        items={navItems}
        style={{ "display": "flex", "justifyContent": "center"}}
        onClick={handleMenuChange}
        selectedKeys={[currentKey]}
        className="menu" />
      <Outlet />
    </div>
  )
}

export default Layout;