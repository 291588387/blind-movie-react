import { TodoList } from './components/TodoList/TodoList';
import { createBrowserRouter } from "react-router-dom";
import Layout from './components/Layout';
import NotFoundPAge from './pages/NotFoundPage';

export const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <Layout />
    ),
    children: [
      {
        path: '/',
        element: <TodoList />
      },
      {
        path: '*',
        element: <NotFoundPAge />
      }
    ]
  },
]);
