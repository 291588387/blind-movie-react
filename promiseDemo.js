function asyncOperation() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const randomNumber = Math.random();
            if (randomNumber > 0.5) {
                resolve(randomNumber);
            } else {
                reject(new Error("Failed"));
            }
        }, 1000);
    });
}

asyncOperation()
    .then(result => {
        console.log("operation success:", result);
    })
    .catch(error => {
        console.log("openration failed:", error);
    });


console.log('******************************************');

function asyncOperation1() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("async1 done");
        resolve(1);
      }, 1000);
    });
  }
  
  function asyncOperation2(result) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("async2 done");
        resolve(result + 2);
      }, 1000);
    });
  }
  

  function asyncOperation3(result) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("async3 done");
        resolve(result + 3);
      }, 1000);
    });
  }
  
  asyncOperation1()
    .then(asyncOperation2)
    .then(asyncOperation3)
    .then(result => {
      console.log("result:", result);
    })
    .catch(error => {
      console.log("failed:", error);
    });